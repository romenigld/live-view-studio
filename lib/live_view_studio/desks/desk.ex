defmodule LiveViewStudio.Desks.Desk do
  use Ecto.Schema
  import Ecto.Changeset

  schema "desks" do
    field :name, :string
    field :photo_urls, {:array, :string}, default: []

    timestamps()
  end

  @doc false
  def changeset(desk, attrs) do
    desk
    |> cast(attrs, [:name])
    |> validate_required([:name])
    |> validate_empty_photo_urls()
  end

  def validate_empty_photo_urls(changeset)  do
    photos = get_field(changeset, :photo_urls)

    if Enum.empty?(photos) do
      add_error(changeset, :photo_urls, "must insert a photo!")
    else
      changeset
    end
  end
end
