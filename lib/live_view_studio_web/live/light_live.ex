defmodule LiveViewStudioWeb.LightLive do
  use LiveViewStudioWeb, :live_view

  def mount(_params, _session, socket) do
    socket = assign(socket, brightness: 10, temp: 3000)

    {:ok, socket}
  end

  def render(assigns) do
    ~L"""
    <h1 class="text-5xl font-bold text-center text-orange-500">Front Porch Light with Tailwind CSS</h1>
    <div id="light" phx-window-keyup="update">
      <form phx-change="change-temp">
        <%= for temp <- [3000, 4000, 5000] do %>
          <%= temp_radio_button(temp: temp, checked: temp == @temp) %>
        <% end %>
      </form>
      <div role="progressbar" class="meter">
        <span style="background-color: <%= temp_color(@temp) %>;
                     width: <%= @brightness %>%">
          <%= @brightness %>%
        </span>
      </div>

      <button phx-click="off">
        <img src="images/light-off.svg">
        <span class="sr-only">Off</span>
      </button>

      <button phx-click="down">
        <img src="images/down.svg">
        <span class="sr-only">Down</span>
      </button>

      <button phx-click="random">
        <img src="images/refresh.svg" alt="Random">
      </button>

      <button phx-click="up">
        <img src="images/up.svg">
        <span class="sr-only">Up</span>
      </button>

      <button phx-click="on" data-role="on">
        <img src="images/light-on.svg">
        <!-- <span class="sr-only">On</span> -->
      </button>

      <p class="py-3 text-3xl font-bold text-blue-600"> Put the Range </p>
      <form phx-change="update">
        <input type="range" min="0" max="100"
               name="brightness" value="<%= @brightness %>" />
      </form>
    </div>

    </br>
    <hr class="mt-8 style-seven">
    </br>

    <h1 class="text-magenta-500">Front Porch Light with custom CSS</h1>

    <div class="meter-blog gradients-blue-green">
      <span style="width: <%= @brightness %>%">
        <%= @brightness %>%
      </span>
    </div>
    <div id="light">
      <button-transparent phx-click="off">
        Off
      </button-transparent>

      <button-transparent phx-click="down">
        Down
      </button-transparent>

      <button-transparent phx-click="random">
        Random
      </button-transparent>

      <button-transparent phx-click="up">
        Up
      </button-transparent>

      <button-transparent phx-click="on">
        On
      </button-transparent>
    </div>

    </br>
    <hr class="mt-8 style-eight">
    </br>
    """
  end

  def handle_event("on", _, socket) do
    socket = assign(socket, :brightness, 100)
    {:noreply, socket}
  end

  def handle_event("off", _, socket) do
    socket = assign(socket, :brightness, 0)
    {:noreply, socket}
  end

  def handle_event("down", _, socket) do
    # socket = update(socket, :brightness, fn b -> b - 10 end)
    socket = update(socket, :brightness, &max(&1 - 10, 0))
    {:noreply, socket}
  end

  def handle_event("random", _, socket) do
    socket = assign(socket, :brightness, Enum.random(0..100))
    {:noreply, socket}
  end

  def handle_event("up", _, socket) do
    # brightness = socket.assigns.brightness + 10
    # socket = assign(socket, :brightness, brightness)
    socket = update(socket, :brightness, &min(&1 + 10, 100))
    {:noreply, socket}
  end

  def handle_event("update", %{"brightness" => brightness}, socket) do
    brightness = String.to_integer(brightness)

    socket =
      assign(socket,
        brightness: brightness)
    {:noreply, socket}
  end


  def handle_event("change-temp", %{"temp" => temp}, socket) do
    temp = String.to_integer(temp)
    socket = assign(socket, temp: temp)

    {:noreply, socket}
  end

  def handle_event("update", %{"key" => "ArrowUp"}, socket) do
    {:noreply, update(socket, :brightness, &min(&1 + 10, 100))}
  end
  def handle_event("update", %{"key" => "ArrowDown"}, socket) do
    {:noreply, update(socket, :brightness, &max(&1 - 10, 0))}
  end
  # Default function clause as a catch-all for other keys.
  def handle_event("update", _, socket), do: {:noreply, socket}

  defp temp_color(3000), do: "#F1C40D"
  defp temp_color(4000), do: "#FEFF66"
  defp temp_color(5000), do: "#99CCFF"

  defp temp_radio_button(assigns) do
    assigns = Enum.into(assigns, %{})

    ~L"""
    <input type="radio" id="<%= @temp %>"
           name="temp" value="<%= @temp %>"
           <%= if @checked, do: "checked" %>
    />
    <label for="<%= @temp %>"><%= @temp %></label>
    """
  end

end
