defmodule LiveViewStudioWeb.PaginateLive do
  use LiveViewStudioWeb, :live_view

  alias LiveViewStudio.Donations

  def mount(_params, _session, socket) do

    {:ok, socket, temporary_assigns: [donations: []]}
  end

  def handle_params(params, _url, socket) do
    page = String.to_integer(params["page"] || "1")
    per_page = String.to_integer(params["per_page"] || "5")

    paginate_options = %{page: page, per_page: per_page}
    donations = Donations.list_donations(paginate: paginate_options)

    socket =
      assign(socket,
        options: paginate_options,
        donations: donations
      )

    {:noreply, socket}
  end

  def render(assigns) do
    ~L"""
    <h1>Food Bank Donations</h1>
    <div id="donations"
         phx-window-keydown="paginate"
         phx-throttle="500">
      <form id="select-per-page" phx-change="select-per-page">
        Show
        <select name="per-page">
          <%= options_for_select([5, 10, 15, 20], @options.per_page) %>
        </select>
        <label for="per-page">per page</label>
      </form>

      <div class="wrapper">
        <table>
          <thead>
            <tr>
              <th class="item">Item</th>
              <th>Quantity</th>
              <th>Days Until Expires</th>
            </tr>
          </thead>
          <tbody>
            <%= for donation <- @donations do %>
              <tr id="donation-<%= donation.id %>">
                <td class="item">
                  <span class="id"><%= donation.id %></span>
                  <%= donation.emoji %> <%= donation.item %>
                </td>
                <td>
                  <%= donation.quantity %> lbs
                </td>
                <td>
                  <span class="<%= expires_class(donation) %>">
                    <%= donation.days_until_expires %>
                  </span>
                </td>
              </tr>
            <% end %>
          </tbody>
        </table>
        <div class="footer">
          <div class="pagination">
            <%= if @options.page > 1 do %>
              <%= pagination_link(@socket,
                                  "Previous",
                                  @options.page - 1,
                                  @options.per_page,
                                  "previous")
              %>
            <% end %>
            <%= for i <- (@options.page - 2)..(@options.page + 2), i > 0 do %>
              <%= pagination_link(@socket,
                                    i,
                                    i,
                                    @options.per_page,
                                    (if i == @options.page, do: "active"))
              %>
            <% end %>
            <%= pagination_link(@socket,
                                "Next",
                                @options.page + 1,
                                @options.per_page,
                                "next")
            %>
          </div>
        </div>
      </div>
    </div>
    """
  end

  def handle_event("select-per-page", %{"per-page" => per_page}, socket) do
    per_page = String.to_integer(per_page)

    socket =
      push_patch(socket,
        to: Routes.live_path(
          socket,
          __MODULE__,
          page: socket.assigns.options.page,
          per_page: per_page
        ))

    {:noreply, socket}
  end

  def handle_event("paginate", %{"key" => "ArrowLeft"}, socket) do
    {:noreply, goto_page(socket, socket.assigns.options.page - 1)}
  end
  def handle_event("paginate", %{"key" => "ArrowRight"}, socket) do
    {:noreply, goto_page(socket, socket.assigns.options.page + 1)}
  end
  # Default function clause as a catch-all for other keys.
  def handle_event("paginate", _, socket), do: {:noreply, socket}

  defp expires_class(donation) do
    if Donations.almost_expired?(donation), do: "eat-now", else: "fresh"
  end

  defp pagination_link(socket, text, page, per_page, class) do
    live_patch text,
      to: Routes.live_path(
          socket,
          __MODULE__,
          page: page,
          per_page: per_page
      ),
      class: class
  end

  defp goto_page(socket, page) when page > 0 do
    push_patch(socket,
      to:
        Routes.live_path(
          socket,
          __MODULE__,
          page: page,
          per_page: socket.assigns.options.per_page
        )
    )
  end

  defp goto_page(socket, _page), do: socket
end
