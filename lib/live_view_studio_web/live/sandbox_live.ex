defmodule LiveViewStudioWeb.SandboxLive do
  use LiveViewStudioWeb, :live_view

  alias LiveViewStudioWeb.QuoteComponent
  alias LiveViewStudioWeb.SandboxCalculatorComponent
  alias LiveViewStudioWeb.DeliveryChargeComponent

  def mount(_params, _session, socket) do
    {:ok, assign(socket, weight: nil, price: nil, delivery_charge: 0)}
  end

  def render(assigns) do
    ~L"""
    <h1>Build a Sandbox</h1>

    <div id="sandbox">
      <%= live_component @socket, SandboxCalculatorComponent,   # stateful
                         id: 1 %>

      <!--
       Alternatively, in the QuoteComponent you could dynamically add a hidden CSS class
       to the surrounding div unless a non-nil weight was passed down, like so:
       <div class="... <%= unless @weight, do: 'hidden' %>">
      -->
      <%= if @weight do %>
        <%= live_component @socket, DeliveryChargeComponent,    # stateful
                           id: 1 %>

        <%= live_component @socket, QuoteComponent,             # stateless
                          material: "sand",
                          weight: @weight,
                          price: @price,
                          color: "pink",
                          delivery_charge: @delivery_charge %>
      <% end %>

    </div>
    """
  end

  def handle_info({SandboxCalculatorComponent, :totals, weight, price}, socket) do
    socket = assign(socket, weight: weight, price: price)
    {:noreply, socket}
  end

  def handle_info({DeliveryChargeComponent, :delivery_charge, charge}, socket) do
    socket = assign(socket, delivery_charge: charge)
    {:noreply, socket}
  end
end
