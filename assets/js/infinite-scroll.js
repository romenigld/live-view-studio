const InfiniteScroll = {
  mounted() {
    console.log("Footer Added to DOM!", this.el)

    // ES6 array destructuring, get the first element of the array of 
    // entries passed to the callback of the IntersectionObserver:
    
    this.observer = new IntersectionObserver(([entry]) => {
                      if (entry.isIntersecting) {
                        this.pushEvent("load-more");
                      }
                    })

    
    // this.observer = new IntersectionObserver(entries => {
    //   const entry = entries[0]
    //   if (entry.isIntersecting) {
    //     console.log("Footer is visible!")
    //     this.pushEvent("load-more")
    //   }
    // })
    

    this.observer.observe(this.el)
  },
  updated() {
    const pageNumber = this.el.dataset.pageNumber
    console.log("updated", pageNumber)
  },
  destroyed() {
    this.observer.disconnect()
  }
}

export default InfiniteScroll