import InfiniteScroll from "./infinite-scroll"
import DatePicker from "./date-picker"
import PhoneNumber from "./phone-number"
import LineChart from "./line-chart"
import IncidentMap from "./incident-map"

let Hooks = {
  InfiniteScroll: InfiniteScroll,
  DatePicker: DatePicker,
  PhoneNumber: PhoneNumber,
  LineChart: LineChart,
  IncidentMap: IncidentMap
}

export default Hooks
