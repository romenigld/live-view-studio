import L from "leaflet";

class IncidentMapping {
  constructor(element, center, markerClickedCallback) {
    this.map = L.map(element).setView(center, 13);

    // Token Generated for the URL: http://localhost:4000/map
    const accessToken = "pk.eyJ1Ijoicm9tZW5pZ2xkIiwiYSI6ImNrbHR2anpycTIwZ3oyb3FtNm0wN2RlM2QifQ.jPpTPiNx0Wk5vrab3kAg_Q";
     // Default public token
    // const accessToken = "pk.eyJ1Ijoicm9tZW5pZ2xkIiwiYSI6ImNrbHIxZjN4bTFpa20ybm4zM2Fjb2JzYmoifQ.pxdKRi_zQfZFD1-TKHi3Bw";

    L.tileLayer(
      "https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}",
      {
        attribution:
          'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
        maxZoom: 18,
        id: "mapbox/streets-v11",
        tileSize: 512,
        zoomOffset: -1,
        accessToken: accessToken,
      }
    ).addTo(this.map);

    this.markerClickedCallback = markerClickedCallback;
  }

  addMarker(incident) {
    const marker =
      L.marker([incident.lat, incident.lng], { incidentId: incident.id })
        .addTo(this.map)
        .bindPopup(incident.description)

    marker.on("click", e => {
      marker.openPopup();
      this.markerClickedCallback(e);
    });

    return marker;
  }

  highlightMarker(incident) {
    const marker = this.markerForIncident(incident);

    marker.openPopup();

    this.map.panTo(marker.getLatLng());
  }

  markerForIncident(incident) {
    let markerLayer;
    this.map.eachLayer(layer => {
      if (layer instanceof L.Marker) {
        const markerPosition = layer.getLatLng();
        if (markerPosition.lat === incident.lat &&
          markerPosition.lng === incident.lng) {
          markerLayer = layer;
        }
      }
    });

    return markerLayer;
  }
}

const IncidentMap = {
  mounted() {
    this.map = new IncidentMapping(this.el, [39.74, -104.99], event => {
      const incidentId = event.target.options.incidentId;

      this.pushEvent("marker-clicked", incidentId, (reply, ref) => {
        this.scrollTo(reply.incident.id)
      })

    // instead of pushing the incidentId, the hook could push the lat/lng values
    // associated with the marker, like so:
    // this.pushEvent("marker-clicked", event.latlng, (reply, ref) => {

    // })
    })

    this.pushEvent("get-incidents", {}, (reply, ref) => {
      reply.incidents.forEach(incident => {
        this.map.addMarker(incident)
      })
    })

    // const incidents = JSON.parse(this.el.dataset.incidents);

    // incidents.forEach(incident => {
    //   this.map.addMarker(incident)
    // })

    this.handleEvent("highlight-marker", incident => {
      this.map.highlightMarker(incident)
    })

    this.handleEvent("add-marker", incident => {
      this.map.addMarker(incident)
      this.map.highlightMarker(incident)
      this.scrollTo(incident.id)
    })
  },

  scrollTo(incidentId) {
    const incidentElement =
      document.querySelector(`[phx-value-id="${incidentId}"]`);

    incidentElement.scrollIntoView(false);
  }
}

export default IncidentMap;
