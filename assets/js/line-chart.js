import Chart from "chart.js";

class LineCharting {
  constructor(ctx, labels, values) {
    this.chart = new Chart(ctx, {
      type: "line",
      data: {
        labels: labels,
        datasets: [
          {
            label: "Wally",
            data: values,
            borderColor: "#4c51bf",
          },
        ],
      },
      options: {
        scales: {
          xAxes: [
            {
              ticks: {
                fontStyle: "bold",
                fontSize: 14,
              },
            },
          ],
          yAxes: [
            {
              ticks: {
                suggestedMin: 50,
                suggestedMax: 200,
                fontStyle: "bold",
                fontSize: 14,
              },
            },
          ],
        },
      },
    });
  }

  addPoint(label, value) {
    const labels = this.chart.data.labels;
    const data = this.chart.data.datasets[0].data;

    labels.push(label);
    data.push(value);

    if (data.length > 12) {
      data.shift();
      labels.shift();
    }

    this.chart.update();
  }
}

const LineChart = {
  mounted() {
    const { labels, values } = JSON.parse(this.el.dataset.chartData)

  /*  Since the hook is now bound to the div rather than the canvas, in the hook's mounted callback
      you'll need to use this.el.firstElementChild to access the canvas element and pass it to the
      LineChart constructor:

    this.chart = new LineChart(this.el.firstElementChild, labels, values)
  */
    this.chart = new LineCharting(this.el, labels, values)

    this.handleEvent("new-point", ({ label, value }) => {
      this.chart.addPoint(label, value)
    })
  }
}

export default LineChart;
