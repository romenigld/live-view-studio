import flatpickr from "flatpickr"
require("flatpickr/dist/themes/material_red.css")

// Define a mounted callback and instantiated a
// flatpickr instance using this.el as the element.
// When a date is picked, use this.pushEvent()
// to push an event to the LiveView with the chosen
// date string as the payload.

const DatePicker = {
  mounted() {
    flatpickr(this.el, {
      enableTime: false,
      dateFormat: "F d, Y",
      onChange: this.handleDatePicked.bind(this),
    })
  },

  handleDatePicked(selectedDates, dateStr, instance) {
    this.pushEvent("dates-picked", dateStr);
  },
}

export default DatePicker