defmodule LiveViewStudioWeb.AutocompleteLiveTest do
  use LiveViewStudioWeb.ConnCase, async: true

  import Phoenix.LiveViewTest

  def store_fixture(attrs) do
    {:ok, store} =
      attrs
      |> Enum.into(%{})
      |> LiveViewStudio.Stores.create_store()

    store
  end

  test "renders", %{conn: conn} do
    {:ok, view, _html} = live(conn, "/autocomplete")

    assert render(view) =~ "Find a Store"
  end

  test "typing in city field suggests a city", %{conn: conn} do
    {:ok, view, _html} = live(conn, "/autocomplete")

    view
    |> form("#city-search", %{city: "D"})
    |> render_change()

    assert has_element?(view, "#matches option", "Denver, CO")
  end

  test "search by city shows matching stores", %{conn: conn} do
    store_fixture(name: "Downtown Denver",
                  street: "426 Aspen Loop",
                  phone_number: "303-555-0140",
                  city: "Denver, CO",
                  zip: "80204",
                  open: true,
                  hours: "8am - 10pm M-F")

    store_fixture(name: "Midtown Denver",
                           street: "7 Broncos Parkway",
                           phone_number: "720-555-0150",
                           city: "Denver, CO",
                           zip: "80204",
                           open: false,
                           hours: "8am - 10pm M-F")
    store_fixture(name: "Denver Stapleton",
                           street: "965 Summit Peak",
                           phone_number: "303-555-0160",
                           city: "Denver, CO",
                           zip: "80204",
                           open: true,
                           hours: "8am - 10pm M-F")

    store_fixture(name: "Denver West",
                           street: "501 Mountain Lane",
                           phone_number: "720-555-0170",
                           city: "Denver, CO",
                           zip: "80204",
                           open: true,
                           hours: "8am - 10pm M-F")

    store_fixture(name: "Westside Helena",
                           street: "734 Lake Loop",
                           phone_number: "406-555-0130",
                           city: "Helena, MT",
                           zip: "59602",
                           open: true,
                           hours: "8am - 10pm M-F")

    {:ok, view, _html} = live(conn, "/autocomplete")

    view
    |> form("#city-search", %{city: "Denver, CO"})
    |> render_submit()

    assert has_element?(view, "li", "Downtown Denver")
    assert has_element?(view, "li", "Midtown Denver")
    assert has_element?(view, "li", "Denver Stapleton")
    assert has_element?(view, "li", "Denver West")
  end

  test "search by city with no results shows error", %{conn: conn} do
    {:ok, view, _html} = live(conn, "/autocomplete")

    view
    |> form("#city-search", %{city: ""})
    |> render_submit()

    assert render(view) =~ "No stores matching"
  end

  test "search by zip shows matching stores", %{conn: conn} do
    store_fixture(name: "Downtown Denver",
                  street: "426 Aspen Loop",
                  phone_number: "303-555-0140",
                  city: "Denver, CO",
                  zip: "80204",
                  open: true,
                  hours: "8am - 10pm M-F")

    store_fixture(name: "Midtown Denver",
                           street: "7 Broncos Parkway",
                           phone_number: "720-555-0150",
                           city: "Denver, CO",
                           zip: "80204",
                           open: false,
                           hours: "8am - 10pm M-F")
    store_fixture(name: "Denver Stapleton",
                           street: "965 Summit Peak",
                           phone_number: "303-555-0160",
                           city: "Denver, CO",
                           zip: "80204",
                           open: true,
                           hours: "8am - 10pm M-F")

    store_fixture(name: "Denver West",
                           street: "501 Mountain Lane",
                           phone_number: "720-555-0170",
                           city: "Denver, CO",
                           zip: "80204",
                           open: true,
                           hours: "8am - 10pm M-F")

    store_fixture(name: "Westside Helena",
                           street: "734 Lake Loop",
                           phone_number: "406-555-0130",
                           city: "Helena, MT",
                           zip: "59602",
                           open: true,
                           hours: "8am - 10pm M-F")

    {:ok, view, _html} = live(conn, "/autocomplete")

    view
    |> form("#zip-search", %{zip: 80204})
    |> render_submit()

    assert has_element?(view, "li", "Downtown Denver")
    assert has_element?(view, "li", "Midtown Denver")
    assert has_element?(view, "li", "Denver Stapleton")
    assert has_element?(view, "li", "Denver West")
  end

  test "search by zip with no results shows error", %{conn: conn} do
    {:ok, view, _html} = live(conn, "/autocomplete")

    view
    |> form("#zip-search", %{zip: 00000})
    |> render_submit()

    assert render(view) =~ "No stores matching"
  end
end
