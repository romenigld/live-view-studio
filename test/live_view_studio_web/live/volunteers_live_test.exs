defmodule LiveViewStudioWeb.VolunteersLiveTest do
  use LiveViewStudioWeb.ConnCase, async: true

  import Phoenix.LiveViewTest
  import LiveViewStudio.AccountsFixtures

  test "renders restricted page if user is logged in", %{conn: conn} do
    view = log_in_user(conn)

    assert render(view) =~ "Volunteer Check-In"
  end

  test "redirects to login page if user is not logged in", %{conn: conn} do
    assert {:error, {:redirect, %{to: "/users/log_in"}}} = live(conn, "/volunteers")
  end

  test "adds valid volunteer to list", %{conn: conn} do
    #{:ok, view, _html} = live(conn, "/volunteers")
    view = log_in_user(conn)

    valid_attrs = %{name: "Name Volunteer", phone: "303-555-1212"}

    view
    |> form("#create-volunteer", %{volunteer: valid_attrs})
    |> render_submit()

    assert has_element?(view, "#volunteers", valid_attrs.name)
  end

  test "display live validations", %{conn: conn} do
    view = log_in_user(conn)

    invalid_attrs = %{name: "Name Volunteer", phone: ""}

    view
    |> form("#create-volunteer", %{volunteer: invalid_attrs})
    |> render_change()

    assert has_element?(view, "#create-volunteer", "can't be blank")
  end

  test "clicking status button toggles status", %{conn: conn} do
    view = log_in_user(conn)

    volunteer= create_volunteer()


    status_button = "#volunteer-#{volunteer.id} button"

    view
    |> element(status_button, "Check Out")
    |> render_click()

    assert has_element?(view, status_button, "Check In")
  end

  test "receives real-time updates", %{conn: conn} do
    view = log_in_user(conn)

    external_volunteer = create_volunteer()

    assert has_element?(view, "#volunteers", external_volunteer.name)
  end

  defp create_volunteer() do
    {:ok, volunteer} =
      LiveViewStudio.Volunteers.create_volunteer(%{
        name: "New Volunteer",
        phone: "303-555-1212"
      })

    volunteer
  end

  defp log_in_user(conn) do
    user = user_fixture()

    {:ok, view, _html} =
      conn
      |> log_in_user(user)
      |> live("/volunteers")

    view
  end
end
