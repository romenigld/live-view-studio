defmodule LiveViewStudioWeb.LicenseLiveTest do
  use LiveViewStudioWeb.ConnCase, async: true

  import Phoenix.LiveViewTest

  test "update number of seats changes seats and amount", %{conn: conn} do
    {:ok, view, _html} = live(conn, "/license")

    # rendered = render(view)
    # assert rendered =~ "2"
    # assert rendered =~ "40,00 €"

    assert has_element?(view, "#seats", "2")
    assert has_element?(view, "#amount", "40,00 €")

# rendered_element =
      view
      |> element("form")
      |> render_change(%{seats: 4})

    # assert rendered_element =~ "4"
    # assert rendered_element =~ "80,00 €"

    assert has_element?(view, "#seats", "4")
    assert has_element?(view, "#amount", "80,00 €")


# rendered_form =
      view
      |> form("#update-seats", %{seats: 6})
      |> render_change()

    # assert rendered_form =~ "6"
    # assert rendered_form =~ "115,00 €"

    assert has_element?(view, "#seats", "6")
    assert has_element?(view, "#amount", "115,00 €")

# using predefined role
      view
      |> form("[role=slider]", %{seats: 10})
      |> render_change()

    assert has_element?(view, "#seats", "10")
    assert has_element?(view, "#amount", "175,00 €")
    assert has_element?(view, "[data-role=seats]", "10")
    assert has_element?(view, "[data-role=amount]", "175,00 €")

  end
end
