defmodule LiveViewStudioWeb.ServersLiveTest do
  use LiveViewStudioWeb.ConnCase, async: true

  import Phoenix.LiveViewTest

  test "clicking a server link shows its details", %{conn: conn} do
    first = create_server("First")
    second = create_server("Second")

    {:ok, view, _html} = live(conn, "/servers")

    assert has_element?(view, "nav", first.name)
    assert has_element?(view, "nav", second.name)
    assert has_element?(view, "#selected-server", second.name)

    view
    |> element("nav a", first.name)
    |> render_click()

    assert has_element?(view, "#selected-server", first.name)
    assert_patched(view, "/servers?id=#{first.id}")
  end

  test "selects the server identified in the URL", %{conn: conn} do
    _first = create_server("First")
    second = create_server("Second")

    {:ok, view, _html} = live(conn, "/servers?id=#{second.id}")

    assert has_element?(view, "#selected-server", second.name)
  end

  test "adds valid server to the nav list", %{conn: conn} do
     _first = create_server("First")

    {:ok, view, _html} = live(conn, "/servers")

    valid_attrs = %{name: "Name Server",
                    size: 3.0,
                    framework: "Phoenix",
                    git_repo: "repo"
                  }

    view
    |> element("#add-server")
    |> render_click()

    assert_patched(view, "/servers/new")

    view
    |> form("#create-server", %{server: valid_attrs})
    |> render_submit()

    assert has_element?(view, "#selected-server", valid_attrs.name)
  end

  test "display live validations", %{conn: conn} do
    {:ok, view, _html} = live(conn, "/servers/new")

    invalid_attrs = %{name: "",
                    size: -1.0,
                    framework: "E",
                    git_repo: "repo"
                  }

    view
    |> form("#create-server", %{server: invalid_attrs})
    |> render_submit()

    assert has_element?(view, "#create-server", "can't be blank")
    assert has_element?(view, "#create-server", "should be at least 2 character(s)")
    assert has_element?(view, "#create-server", "can't be blank")
    assert has_element?(view, "#create-server", "must be greater than 0")
  end

  test "clicking status button toggles server's status on and off", %{conn: conn} do
    server = create_server("Name Server")

    {:ok, view, _html} = live(conn, "/servers")

    status_server = "#server-#{server.id} button"

    view
    |> element(status_server, "up")
    |> render_click()

    assert has_element?(view, status_server, "down")
  end

  test "receives real-time updates", %{conn: conn} do
    _first = create_server("First")

    {:ok, view, _html} = live(conn, "/servers")

    external_server = create_server("External Server")

    assert has_element?(view, "#servers", external_server.name)
  end

  defp create_server(name) do
    {:ok, server} =
      LiveViewStudio.Servers.create_server(%{
        name: name,
        # these are irrelevant for tests:
        status: "up",
        deploy_count: 1,
        size: 1.0,
        framework: "framework",
        git_repo: "repo",
        last_commit_id: "id",
        last_commit_message: "message"
      })

    server
  end
end
